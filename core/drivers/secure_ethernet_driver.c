#include <kernel/panic.h>
#include <printk.h>
#include <kernel/thread.h>
#include <kernel/pseudo_ta.h>
#include <kernel/ts_manager.h>
#include <kernel/tee_ta_manager.h>
#include <kernel/interrupt.h>
#include <kernel/notif.h>
#include <kernel/trace_ta.h>
#include <string.h>
#include <sys/queue.h>
#include <initcall.h>
#include <string.h>
#include <malloc.h>
#include <mm/core_memprot.h>

#include <drivers/secure_ethernet_driver.h>
#include <drivers/secure_ethernet_driver_public.h>
#include <drivers/secure_controller.h>
#include <drivers/spi_eth.h>
#include <drivers/ecspi_ifc.h>

#include <io.h>
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>

/**
 * sizeof_field(TYPE, MEMBER)
 *
 * @TYPE: The structure containing the field of interest
 * @MEMBER: The field to return the size of
 */
#define sizeof_field(TYPE, MEMBER) sizeof((((TYPE *)0)->MEMBER))

#define ARCH_DMA_MINALIGN 64
#define MAX_PACKETS	  10

#define SEC_ETH_DRIVER_UUID                                               \
	{                                                                 \
		UUID1, UUID2, UUID3,                                      \
		{                                                         \
			UUID4, UUID5, UUID6, UUID7, UUID8, UUID9, UUID10, \
				UUID11                                    \
		}                                                         \
	}
static TEE_UUID sec_eth_driver_uuid = SEC_ETH_DRIVER_UUID;

struct buf_entry {
	uint8_t *pkt;
	uint16_t pkt_len;
	STAILQ_ENTRY(buf_entry) queue;
};

struct sec_eth_packet {
	uint16_t len;
	uint8_t data[];
};

struct sec_eth_buffer {
	uint8_t buf[1024 * 16 * sizeof(uint8_t)];
	unsigned int size;
	unsigned int read;
	unsigned int write;
	unsigned int watermark;
};

static struct device {
	unsigned int pkt_cnt;
	struct sec_eth_buffer *rx_buffer;
	struct sec_eth_buffer *tx_buffer;
	STAILQ_HEAD(buf_head, buf_entry) buffer;
} *norm_dev;

// INIT parameter types
static uint32_t eth_init_param =
	TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE,
			TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

typedef enum {
	DEST_SEC,
	DEST_NORM,
	DEST_BOTH,
	DEST_BRDCST,
} pkt_dest;

// static struct imx_fec_data fec_data;

static uint8_t mac_secure[6] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
static uint8_t mac_normal[6] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };
static uint8_t mac_brdcst[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

static uint8_t ip[4] = { 192, 168, 0, 101 };
static uint8_t gateway[4] = { 192, 168, 0, 254 };
static uint8_t subnet[4] = { 255, 255, 255, 0 };

static enum itr_return eth_itr_cb(struct itr_handler *handler __unused);

static struct itr_handler eth_itr = {
	.it = 105,
	.flags = ITRF_TRIGGER_LEVEL,
	.handler = eth_itr_cb,
};

#define REGS_GPIO4_BASE 0x020a8000
static struct io_pa_va pbase_gpio = { .pa = REGS_GPIO4_BASE };

#define GPIO4_DR_OFFSET	      0x0
#define GPIO4_GDIR_OFFSET     0x4
#define GPIO4_PSR_OFFSET      0x8
#define GPIO4_ICR1_OFFSET     0xc
#define GPIO4_ICR2_OFFSET     0x10
#define GPIO4_IMR_OFFSET      0x14
#define GPIO4_ISR_OFFSET      0x18
#define GPIO4_EDGE_SEL_OFFSET 0x1c
#define GPIO_ICR_PIN29_SHIFT  26

static void enable_gpio_interrupt(void)
{
	vaddr_t base = io_pa_or_va(&pbase_gpio, 0x4000);
	// enable GPIO4_PIN29 as input
	io_clrbits32(base + GPIO4_GDIR_OFFSET, 0b1 << 29);

	// make GPIO4_ICR29 falling edge sensitive
	io_clrbits32(base + GPIO4_ICR2_OFFSET, 0b11 << GPIO_ICR_PIN29_SHIFT);

	// unmask GPIO4_ICR29
	io_setbits32(base + GPIO4_IMR_OFFSET, 0b1 << 29);

	// Set up external trigger (GPIO4_PIN30)
	io_setbits32(base + GPIO4_GDIR_OFFSET, 0b1 << 30);

	DMSG("GDIR: %x", io_read32(base + GPIO4_GDIR_OFFSET));
	DMSG("ICR2: %x", io_read32(base + GPIO4_ICR2_OFFSET));
	DMSG("IMR: %x", io_read32(base + GPIO4_IMR_OFFSET));
}

static TEE_Result register_itr(void)
{
	itr_add(&eth_itr);
	itr_set_affinity(eth_itr.it, 1);
	itr_set_priority(eth_itr.it, 24);
	enable_gpio_interrupt();

	return TEE_SUCCESS;
}

static TEE_Result unregister_itr(void)
{
	itr_disable(eth_itr.it);

	return TEE_SUCCESS;
}

static void print_packet(const uint8_t *const packets, const uint16_t len)
{
	uint16_t i;
	int j;

	for (i = 0; i < len; i += 16) {
		char buf[512];
		size_t boffs = 0;
		int res;
		res = snprintk(buf, sizeof(buf), "%04x: ", i);

		for (j = 0; j < 16; j++) {
			boffs += res;
			if (i + j >= len) {
				goto out;
			}
			res = snprintk(buf + boffs, sizeof(buf) - boffs,
				       "%02x ", packets[i + j]);
			if (j == 7) {
				buf[boffs + res] = ' ';
				res++;
			}
		}

		boffs += res;
		buf[boffs] = ' ';
		buf[boffs + 1] = ' ';
		buf[boffs + 2] = ' ';

		for (j = 0; j < 16; j++) {
			boffs += res;
			if (packets[i + j] > 31 && packets[i + j] < 127) {
				res = snprintk(buf + boffs, sizeof(buf) - boffs,
					       "%c", packets[i + j]);
			} else {
				buf[boffs] = '.';
				res = 1;
			}

			if (j == 7) {
				buf[boffs + res] = ' ';
				res++;
			}
		}
		boffs += res;

	out:
		if (boffs >= (sizeof(buf) - 1))
			boffs = sizeof(buf) - 2;

		buf[boffs] = '\n';
		while (boffs && buf[boffs] == '\n')
			boffs--;
		boffs++;
		buf[boffs + 1] = '\0';

		trace_ext_puts(buf);
	}
}

// clang-format off
// static uint8_t icmp_ping[] = {
// 	/*000*/ 0x00, 0x24, 0x32, 0x00, 0x12, 0x24, // MAC Dest
// 	/*006*/ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, // MAC Src
// 	/*012*/ 0x08, 0x00, // Type IPv4
// 	        // IPv4 Packet:
// 	/*014*/ 0x45, //IPv4 version & Header length
// 	/*015*/ 0x00, // Services
// 	/*016*/ 0x00, 0x54, // Total length
// 	/*018*/ 0xab, 0xe4, // Identification
// 	/*020*/ 0x40, 0x00, // Flags & fragmentation offset
// 	/*022*/ 0x40, // TTL
// 	/*023*/ 0x01, // Protocol
// 	/*024*/ 0x0d, 0x0f, // Header checksum
// 	/*026*/ 0xc0, 0xa8, 0x00, 0x65, // Source Address
// 	/*030*/ 0xc0, 0xa8, 0x00, 0x01, // Destination Address
// 	        // ICMP packet:
// 	/*034*/ 0x08, // ICMP type
// 	/*035*/ 0x00, // ICMP Code
// 	/*036*/ 0x14, 0xac, // ICMP Checksum
// 	/*038*/ 0x01, 0x08, // Identifier
// 	/*040*/ 0x00, 0x01, // Sequence Number
// 	        // ICMP Payload:
// 	/*042*/ 0x19, 0x28, 0xc9, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 	/*052*/ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 	/*062*/ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 	/*072*/ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 	/*082*/ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 	/*092*/ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// };
// clang-format on

static pkt_dest check_packet_destination(const uint8_t *const pkt)
{
	if (memcmp(pkt, mac_secure, 6) == 0) {
		return DEST_SEC;
	} else if (memcmp(pkt, mac_normal, 6) == 0) {
		return DEST_NORM;
	} else if (memcmp(pkt, mac_brdcst, 6) == 0) {
		return DEST_BRDCST;
	} else {
		return DEST_BOTH;
	}
}

#define GPT_BASE 0x02098000
#define GPT_CR	 0x0
#define GPT_PR	 0x4
#define GPT_CNT	 0x24
static struct io_pa_va gpt_base = { .pa = GPT_BASE };
static uint32_t src_freq = 24000000 / 8;
static uint32_t freq = 0;
static uint32_t start;
static uint32_t notify_start;

static void setup_timer(void)
{
	vaddr_t base = io_pa_or_va(&gpt_base, 0x100);

	uint32_t control = io_read32(base + GPT_CR);
	DMSG("control: %x", control);
	if (((control >> 6) & 0b111) != 0x5) {
		EMSG("Clock source is not the 24M crystal clock!!!");
		panic("Wrong clock source!");
	}

	uint32_t prescaler = io_read32(base + GPT_PR);
	freq = src_freq / (((prescaler >> 12) & 0xf) + 1) /
	       ((prescaler & 0xfff) + 1);
	IMSG("Freq: %u", freq);
}

static unsigned int buffer_free(struct sec_eth_buffer *buf)
{
	if (buf->write >= buf->read) {
		return buf->size - buf->write;
	} else {
		return buf->read - buf->write;
	}
}

static int copy_to_buffer(struct sec_eth_buffer *buf, void *data,
			  unsigned int len, unsigned int pkt_len)
{
	unsigned int free = buffer_free(buf);
	unsigned int len_in_buf =
		pkt_len + sizeof_field(struct sec_eth_packet, len);
	struct sec_eth_packet *pkt;

	// Fail if buffer is full.
	// Deal with wraparound now
	if (buf->write >= buf->read && free < len_in_buf &&
	    buf->read <= len_in_buf) { // Don't overtake read index
		return -1;
	}

	// Don't overtake read index by not allowing free to equal length.
	if (buf->write < buf->read && free <= len_in_buf) {
		return -1;
	}

	// Shift to start of buffer if too small.
	if (free < len_in_buf) {
		buf->watermark = buf->write;
		buf->write = 0;
	}

	pkt = (struct sec_eth_packet *)&(buf->buf[buf->write]);

	memcpy(pkt->data, data, len);

	// packet should be padded with 0
	if (len < pkt_len) {
		memset(pkt->data + len, 0, pkt_len - len);
	}

	pkt->len = pkt_len;
	buf->write += len_in_buf;

	return pkt_len;
}

// static TEE_Result ping_test(void)
// {
// 	TEE_Result ret = TEE_SUCCESS;
// 	unsigned int i = 0;

// 	vaddr_t base = io_pa_or_va(&gpt_base, 0x100);
// 	if (freq == 0) {
// 		setup_timer();
// 	}

// 	icmp_ping[19] += 1;
// 	icmp_ping[41] += 1;

// 	icmp_ping[24] = 0;
// 	icmp_ping[25] = 0;
// 	uint32_t checksum = 0;
// 	for (i = 14; i < 34; i += 2) {
// 		checksum += *(uint16_t *)&icmp_ping[i];
// 	}
// 	checksum = ~((checksum & 0xffff) + (checksum >> 16));
// 	icmp_ping[24] = checksum & 0xff;
// 	icmp_ping[25] = (checksum >> 8) & 0xff;

// 	icmp_ping[36] = 0;
// 	icmp_ping[37] = 0;
// 	checksum = 0;
// 	for (i = 34; i < 98; i += 2) {
// 		checksum += *(uint16_t *)&icmp_ping[i];
// 	}
// 	checksum = ~((checksum & 0xffff) + (checksum >> 16) + (checksum >> 16));
// 	icmp_ping[36] = checksum & 0xff;
// 	icmp_ping[37] = (checksum >> 8) & 0xff;

// 	start = io_read32(base + GPT_CNT);
// 	ret = send_packet((uint8_t *)icmp_ping, sizeof(icmp_ping));
// 	if (ret) {
// 		EMSG("Problem sending ping request!");
// 	}

// 	return ret;
// }

static uint32_t ping_start[1000] = {};
static uint32_t ping_end[1000] = {};
static unsigned int m = 0;
static unsigned int n = 0;

static TEE_Result ping_test(void)
{
	unsigned int i = 0;
	IMSG("Ping timings:\n");
	for (i; i < n; i++) {
		// ping_start[m] = (end - start) / (freq / 1000000);
		IMSG("%3u: %8u µs\n", i,
		     (ping_end[i] - ping_start[i]) / (freq / 1000000));
	}

	m = n = 0;

	return TEE_SUCCESS;
}

static enum itr_return eth_itr_cb(struct itr_handler *handler __unused)
{
	uint8_t *packets;
	uint16_t len;
	pkt_dest dest;
	struct sec_eth_buffer *buf = norm_dev->rx_buffer;
	int ret;
	uint32_t end;
	int pings = 0;

	vaddr_t timer_base = io_pa_or_va(&gpt_base, 0x100);
	if (freq == 0) {
		setup_timer();
	}

	vaddr_t base = io_pa_or_va(&pbase_gpio, 0x4000);
	if (!(io_read32(base + GPIO4_ISR_OFFSET) & (0b1 << 29))) {
		DMSG("Not our interrupt!");
		return ITRR_NONE;
	}

	recv_packets(&packets, &len);

	uint16_t i = 0;
	while (i < len) {
		uint16_t size = packets[i] << 8 | packets[i + 1];
		size -= 2;
		i += 2;

		dest = check_packet_destination(&packets[i]);
		if ((dest == DEST_NORM || dest == DEST_BRDCST) && norm_dev) {
			if (copy_to_buffer(buf, &packets[i], size, size) < 0) {
				DMSG("Normal world buffer is full!");
				continue;
			}
			if (packets[i + 23] == 0x1) {
				// ICMP packet
				pings++;
			}
		}
		if (dest == DEST_SEC) {
			// print_packet(&packets[i], size);
		} else {
			// Drop packet.
		}

		i += size;
	}

	// Send notification to normal world that a frame has been received.
	if (buf->read != buf->write) {
		// notify_start = io_read32(timer_base + GPT_CNT);
		// IMSG("notify_start: %u", notify_start);
		TEE_Result res = sec_ssp_notify_fast();
		if (res) {
			EMSG("Error sending notification.\n");
		}
	}

	free(packets);

	reset_interrupt();
	io_clrbits32(base + GPIO4_ISR_OFFSET, 0b1 << 29);

	end = io_read32(timer_base + GPT_CNT);
	for (i = 0; i < pings; i++) {
		ping_end[n] = end;
		n++;
	}

	return ITRR_HANDLED;
}

static TEE_Result init_normal_world(uint32_t param_types,
				    TEE_Param params[TEE_NUM_PARAMS])
{
	// Verify incoming parameter types
	uint32_t expected_types =
		TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INOUT,
				TEE_PARAM_TYPE_MEMREF_INOUT,
				TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);
	if (param_types != expected_types) {
		EMSG("ERROR PARAMETERS %d -- %d ", param_types, expected_types);
		return TEE_ERROR_BAD_PARAMETERS;
	}

	if (!norm_dev) {
		norm_dev = malloc(sizeof(struct device));
		memset(norm_dev, 0, sizeof(struct device));

		STAILQ_INIT(&norm_dev->buffer);
	}

	// Setup shared memory buffers
	norm_dev->rx_buffer =
		core_mmu_add_mapping(MEM_AREA_RAM_NSEC,
				     virt_to_phys(params[0].memref.buffer),
				     params[0].memref.size);
	norm_dev->tx_buffer =
		core_mmu_add_mapping(MEM_AREA_RAM_NSEC,
				     virt_to_phys(params[1].memref.buffer),
				     params[1].memref.size);

	return TEE_SUCCESS;
}

static TEE_Result create_entry_point(void)
{
	TEE_Result ret;

	setup_timer();

	if ((ret = open_ethernet())) {
		EMSG("Couldn't open ethernet: %x", ret);
		return ret;
	}

	if ((ret = register_itr())) {
		EMSG("Couldn't register interrupt: %x", ret);
		return ret;
	}

	if ((ret = configure_ethernet(mac_secure, ip, gateway, subnet))) {
		EMSG("Couldn't configure ethernet: %x", ret);
		return ret;
	}

	itr_enable(eth_itr.it);
	DMSG("Enabled interrupt");

	return TEE_SUCCESS;
}

static bool sending = false;

TEE_Result secure_ethernet_driver_send(void)
{
	TEE_Result ret = TEE_SUCCESS;
	vaddr_t base = io_pa_or_va(&pbase_gpio, 0x4000);
	vaddr_t timer_base = io_pa_or_va(&gpt_base, 0x100);
	if (freq == 0) {
		setup_timer();
	}

	if (io_read32(base + GPIO4_ISR_OFFSET) & 0b1 << 29 || sending) {
		return TEE_ERROR_BUSY;
	}

	sending = true;
	itr_disable(eth_itr.it);

	// Check again to take care of race conditions.
	if (io_read32(base + GPIO4_ISR_OFFSET) & 0b1 << 29) {
		ret = TEE_ERROR_BUSY;
		goto out;
	}

	struct sec_eth_buffer *buf = norm_dev->tx_buffer;
	while (buf->read != buf->write) {
		if (buf->read == buf->watermark) {
			// Reset buffer to start.
			buf->read = 0;
			buf->watermark = buf->size;
		}

		struct sec_eth_packet *pkt =
			(struct sec_eth_packet *)&(buf->buf[buf->read]);

		// DMSG("Send packet length: %u", pkt->len);
		// print_packet(pkt->data, pkt->len);

		if (pkt->data[23] == 0x1) {
			ping_start[m] = io_read32(timer_base + GPT_CNT);
			m++;
		}

		ret = send_packet(pkt->data, pkt->len);
		if (ret) {
			EMSG("send_packet failed with error code %x", ret);
			goto out;
		}

		buf->read +=
			sizeof_field(struct sec_eth_packet, len) + pkt->len;

		ret = TEE_SUCCESS;
	}

out:
	itr_enable(eth_itr.it);
	sending = false;
	return ret;
}

static TEE_Result invoke_command(void *pSessionContext __unused,
				 uint32_t nCommandID, uint32_t nParamTypes,
				 TEE_Param pParams[TEE_NUM_PARAMS])
{
	switch (nCommandID) {
	case SEC_ETH_CMD_SEND:
		return secure_ethernet_driver_send();

	case SEC_ETH_CMD_INIT:
		DMSG("Invoking print function: 4382b90a34ac\n");
		return init_normal_world(nParamTypes, pParams);

	case SEC_ETH_CMD_PING:
		if (nParamTypes != eth_init_param) {
			EMSG("ERROR PARAMETERS %d -- %d ", nParamTypes,
			     eth_init_param);
			return TEE_ERROR_BAD_PARAMETERS;
		}

		return ping_test();

	default:
		break;
	}

	return TEE_ERROR_NOT_IMPLEMENTED;
}

pseudo_ta_register(.uuid = SEC_ETH_DRIVER_UUID, .name = PTA_NAME,
		   .flags = PTA_DEFAULT_FLAGS,
		   .invoke_command_entry_point = invoke_command,
		   .create_entry_point = create_entry_point);

// service_init_late(enable_gpio_interrupt);