#include <kernel/panic.h>
#include <drivers/spi_eth.h>
#include <mm/core_memprot.h>
#include <io.h>
#include <trace.h>
#include <drivers/ecspi_ifc.h>
#include <drivers/regsecspi.h>
#include <tee_api_defines.h>

static struct io_pa_va ecspi_base = { .pa = REGS_ECSPI1_BASE };

#define ECSPI_FIFO_SIZE 64

#define SPI_RETRY_TIMES 100

#define REGS_IOMUXC_BASE 0x020e0000

#define MUX_CTL_DISP0_DATA22_OFFSET	0x1c8
#define PAD_CTL_DISP0_DATA22_OFFSET	0x4dc
#define ECSPI1_MISO_INPUT_SELECT_OFFSET 0x7f8

#define MUX_CTL_DISP0_DATA21_OFFSET	0x1c4
#define PAD_CTL_DISP0_DATA21_OFFSET	0x4d8
#define ECSPI1_MOSI_INPUT_SELECT_OFFSET 0x7fc

#define MUX_CTL_DISP0_DATA20_OFFSET    0x1c0
#define PAD_CTL_DISP0_DATA20_OFFSET    0x4d4
#define ECSPI1_CLK_INPUT_SELECT_OFFSET 0x7f4

#define MUX_CTL_DISP0_DATA23_OFFSET    0x1cc
#define PAD_CTL_DISP0_DATA23_OFFSET    0x4e0
#define ECSPI1_SS0_INPUT_SELECT_OFFSET 0x800

#define MUX_CTL_DISP0_DATA08_OFFSET 0x190
#define PAD_CTL_DISP0_DATA08_OFFSET 0x4a4

#define MUX_CTL_DISP0_DATA09_OFFSET 0x194
#define PAD_CTL_DISP0_DATA09_OFFSET 0x4a8

static struct io_pa_va pbase = { .pa = REGS_IOMUXC_BASE };
static vaddr_t offsets[4][3] = {
	{
		MUX_CTL_DISP0_DATA22_OFFSET,
		PAD_CTL_DISP0_DATA22_OFFSET,
		ECSPI1_MISO_INPUT_SELECT_OFFSET,
	},
	{
		MUX_CTL_DISP0_DATA21_OFFSET,
		PAD_CTL_DISP0_DATA21_OFFSET,
		ECSPI1_MOSI_INPUT_SELECT_OFFSET,
	},
	{
		MUX_CTL_DISP0_DATA20_OFFSET,
		PAD_CTL_DISP0_DATA20_OFFSET,
		ECSPI1_CLK_INPUT_SELECT_OFFSET,
	},
	{
		MUX_CTL_DISP0_DATA23_OFFSET,
		PAD_CTL_DISP0_DATA23_OFFSET,
		ECSPI1_SS0_INPUT_SELECT_OFFSET,
	},
};

TEE_Result configure_iomux(void)
{
	if (core_pbuf_is(CORE_MEM_CACHED, pbase.pa, 0x4000)) {
		EMSG("The memory of IOMUXC is cached!");
		return TEE_ERROR_BAD_STATE;
	}

	DMSG("Configuring iomux for spi");

	unsigned int i;
	vaddr_t base = io_pa_or_va(&pbase, 0x4000);

	for (i = 0; i < 4; i++) {
		io_write32(base + offsets[i][0], 0x2); // DISABLED, ALT2
		// ENABLED, 100K_OHM_PD, KEEP, DISABLED, DISABLED, 100MHZ, 40_OHM, FAST
		io_write32(base + offsets[i][1], 0x100b1);
		io_write32(base + offsets[i][2], 0x1); // DISP0_DATA22_ALT2
	}

	// Enable pull up gpio4_io29 input
	io_write32(base + MUX_CTL_DISP0_DATA08_OFFSET, 0x5); // DISABLED, ALT5
	// HYS enabled, pull-up & -down disabled, keeper disabld, slowest speed, HI-Z
	io_write32(base + PAD_CTL_DISP0_DATA08_OFFSET, 0x10000);

	// Enable output gpio4_io30
	io_write32(base + MUX_CTL_DISP0_DATA09_OFFSET, 0x5);
	io_write32(base + PAD_CTL_DISP0_DATA09_OFFSET, 0x1a0f1);

	return TEE_SUCCESS;
}

TEE_Result verify_iomux(void)
{
	unsigned int i;
	vaddr_t base = io_pa_or_va(&pbase, 0x4000);

	for (i = 0; i < 4; i++) {
		uint32_t diff = 0;
		diff |= 0x2 & ~io_read32(base + offsets[i][0]);
		diff |= 0x100b1 & ~io_read32(base + offsets[i][1]);
		diff |= 0x1 & ~io_read32(base + offsets[i][2]);

		if (diff) {
			EMSG("IOMUX offset %u is wrong!", i);
			return TEE_ERROR_BAD_STATE;
		}
	}

	return TEE_SUCCESS;
}

TEE_Result configure_clocks(void)
{
	vaddr_t base =
		io_pa_or_va(&(struct io_pa_va){ .pa = 0x020c4000 }, 0x4000);

	io_setbits32(base + 0x6c, 0b1111111111);

	return TEE_SUCCESS;
}

#define GPT_BASE 0x02098000
#define GPT_CR	 0x0
#define GPT_PR	 0x4
#define GPT_CNT	 0x24
static struct io_pa_va gpt_base = { .pa = GPT_BASE };
static uint32_t src_freq = 24000000 / 8;
static uint32_t freq = 0;

static void udelay(uint32_t us)
{
	vaddr_t base = io_pa_or_va(&gpt_base, 0x100);
	if (freq == 0) {
		uint32_t control = io_read32(base + GPT_CR);

		if (!control & 0x1) {
			// Enable global timer.
			io_setbits32(base + GPT_CR, 0x1);
		}

		if (((control >> 6) & 0b111) != 0x5) {
			EMSG("Clock source is not the 24M crystal clock!!!");
			panic("Wrong clock source!");
		}

		uint32_t prescaler = io_read32(base + GPT_PR);
		freq = src_freq / (((prescaler >> 12) & 0xf) + 1) /
		       ((prescaler & 0xfff) + 1);
		DMSG("Final frequency: %u", freq);
	}

	isb();
	uint32_t start = io_read32(base + GPT_CNT);
	uint32_t target = start + us * (freq / 1000000);

	uint32_t current;
	while ((current = io_read32(base + GPT_CNT)) <= target) {
	}
}

static void ecspi_start_transfer(vaddr_t base, uint16_t brs_bts)
{
	// Set burst length
	HW_ECSPI_CONREG(base).B.BURST_LENGTH = brs_bts - 1;

	// Clear status
	HW_ECSPI_STATREG_WR(base, BM_ECSPI_STATREG_RO | BM_ECSPI_STATREG_TC);

	// Wait for 5 µs (two clock cycles) to propagate changes.
	udelay(5);
}

int ecspi_configure(vaddr_t base, const param_ecspi_t *param)
{
	// Reset eCSPI controller
	HW_ECSPI_CONREG(base).B.EN = 0;

	// Setup chip select
	HW_ECSPI_CONREG(base).B.CHANNEL_SELECT = param->channel;

	// Setup mode
	uint32_t channelMask = 1 << param->channel;
	uint32_t value = HW_ECSPI_CONREG(base).B.CHANNEL_MODE;
	BW_ECSPI_CONREG_CHANNEL_MODE(base, param->mode ?
						   (value | channelMask) :
						   (value & ~channelMask));

	// Setup pre & post clock divider
	HW_ECSPI_CONREG(base).B.PRE_DIVIDER =
		(param->pre_div == 0) ? 0 : (param->pre_div - 1);
	HW_ECSPI_CONREG(base).B.POST_DIVIDER = param->post_div;

	// Enable eCSPI
	HW_ECSPI_CONREG(base).B.EN = 1;

	// Setup SCLK_PHA, SCLK_POL, SS_POL
	value = HW_ECSPI_CONFIGREG(base).B.SCLK_PHA;
	HW_ECSPI_CONFIGREG(base).B.SCLK_PHA = param->sclk_pha ?
						      (value | channelMask) :
						      (value & ~channelMask);

	value = HW_ECSPI_CONFIGREG(base).B.SCLK_POL;
	HW_ECSPI_CONFIGREG(base).B.SCLK_POL = param->sclk_pol ?
						      (value | channelMask) :
						      (value & ~channelMask);

	value = HW_ECSPI_CONFIGREG(base).B.SS_POL;
	HW_ECSPI_CONFIGREG(base).B.SS_POL =
		param->ss_pol ? (value | channelMask) : (value & ~channelMask);

	HW_ECSPI_CONFIGREG(base).B.SS_CTL |= channelMask;

	// Wait for 5 µs (two clock cycles) to propagate changes.
	udelay(5);

	return TEE_SUCCESS;
}

int ecspi_open(vaddr_t base, const param_ecspi_t *param)
{
	configure_clocks();
	configure_iomux();
	verify_iomux();

	// Configure eCSPI registers
	ecspi_configure(base, param);

	return TEE_SUCCESS;
}

int ecspi_close(vaddr_t base)
{
	// Disable controller
	HW_ECSPI_CONREG(base).B.EN = 0;

	return TEE_SUCCESS;
}

static uint32_t ecspi_read_rx(vaddr_t base)
{
	// Wait for data ready
	while (!HW_ECSPI_STATREG(base).B.RR) {
	}
	return HW_ECSPI_RXDATA_RD(base);
}

static void ecspi_write_tx(vaddr_t base, uint32_t val)
{
	HW_ECSPI_TXDATA_WR(base, val);
}

uint16_t ecspi_read(uint8_t command, uint16_t mem_offset,
		    volatile uint8_t *const buf, uint16_t bytes)
{
	vaddr_t base = io_pa_or_va(&ecspi_base, 0x4000);

	size_t buf_offset = 0;

	while (bytes > 0) {
		uint32_t cmd = (mem_offset + buf_offset) << 16 | command << 8;
		uint8_t cmd_length = 3;
		uint16_t current_bytes =
			bytes <= (ECSPI_FIFO_SIZE * 4) - cmd_length ?
				bytes :
				(ECSPI_FIFO_SIZE * 4) - cmd_length;
		bytes -= current_bytes;
		unsigned int sent = 0;

		ecspi_start_transfer(base, (cmd_length + current_bytes) * 8);

		unsigned int overflow = (cmd_length + current_bytes) % 4;
		unsigned int offset =
			(cmd_length % 4) - overflow; // minimum 1, max 4

		if (overflow > 0) {
			ecspi_write_tx(base, cmd >> (4 - overflow) * 8);
			sent += overflow;
		}
		ecspi_write_tx(base, cmd << overflow * 8);
		sent += 4;
		// The amount of bytes that got sent along with the command
		unsigned int sent_along = 4 - ((cmd_length % 4) - overflow);
		for (unsigned int i = 0; i < (current_bytes - sent_along) / 4;
		     i++) {
			ecspi_write_tx(base, 0x0);
			sent += 4;
		}

		// Drain receive buffer
		while (HW_ECSPI_STATREG(base).B.RR) {
			HW_ECSPI_RXDATA_RD(base);
		}

		HW_ECSPI_CONREG_SET(base, BM_ECSPI_CONREG_XCH);

		// Wait for transfer complete
		while (HW_ECSPI_CONREG(base).B.XCH) {
			continue;
		}

		uint32_t data;
		if (overflow > 0) {
			ecspi_read_rx(base);
		}
		if (offset > 0) {
			data = ecspi_read_rx(base);
		}
		unsigned int i = 0;
		for (; i < current_bytes; i++) {
			if ((i + offset) % 4 == 0) {
				data = ecspi_read_rx(base);
			}

			/* Data is stored MSB first in each word:
			 *
			 *            byte 4
			 *            byte 5
			 *            byte 6
			 * data[1] -> byte 7
			 *            byte 0
			 *            byte 1 <- ...
			 *            byte 2 <- (uint8_t *)&data[1]
			 * data[0] -> byte 3 <- (uint8_t *)&data[0]
			 *            ...
			*/
			buf[buf_offset + i] =
				((uint8_t *)&data)[3 - ((i + offset) % 4)];
		}

		buf_offset += current_bytes;
	}

	return buf_offset;
}

uint16_t ecspi_write(uint8_t command, uint16_t mem_offset, uint8_t *const buf,
		     uint16_t bytes)
{
	vaddr_t base = io_pa_or_va(&ecspi_base, 0x4000);

	size_t buf_offset = 0;

	while (bytes > 0) {
		uint8_t cmd_length = 3;
		uint32_t cmd =
			(mem_offset + buf_offset) << 16 | command << 8 | buf[0];
		uint16_t current_bytes =
			bytes <= (ECSPI_FIFO_SIZE * 4) - cmd_length ?
				bytes :
				(ECSPI_FIFO_SIZE * 4) - cmd_length;
		bytes -= current_bytes;
		unsigned int sent = 0;

		ecspi_start_transfer(base, (cmd_length + current_bytes) * 8);

		unsigned int overflow = (cmd_length + current_bytes) % 4;

		if (overflow > 0) {
			ecspi_write_tx(base, cmd >> (4 - overflow) * 8);
			sent += overflow;
		}

		unsigned int i = 1;
		switch (overflow) {
		case 3:
			cmd = cmd << 8 | buf[i++];
			fallthrough;
		case 2:
			cmd = cmd << 8 | buf[i++];
			fallthrough;
		case 1:
			cmd = cmd << 8 | buf[i++];
			fallthrough;
		default:
			ecspi_write_tx(base, cmd);
			sent += 4;
			break;
		}

		uint32_t data = 0;
		unsigned int offset = i;
		for (; i < current_bytes; i++) {
			data <<= 8;
			data |= buf[buf_offset + i];

			if ((i - offset + 1) % 4 == 0) {
				ecspi_write_tx(base, data);
				data = 0;
				sent += 4;
			}
		}

		if (sent < current_bytes + cmd_length) {
			ecspi_write_tx(base, data);
		}

		HW_ECSPI_CONREG_SET(base, BM_ECSPI_CONREG_XCH);

		// Wait for transfer complete
		while (HW_ECSPI_CONREG(base).B.XCH) {
			continue;
		}

		// Read all incoming words as a reliable way of telling the message has finished transmitting.
		for (i = 0; i < (cmd_length + current_bytes + 3) / 4; i++) {
			ecspi_read_rx(base);
		}

		buf_offset += current_bytes;
	}

	return buf_offset;
}

#define REGS_GPIO4_BASE 0x020a8000
#define GPIO4_DR_OFFSET 0x0
static struct io_pa_va pbase_gpio = { .pa = REGS_GPIO4_BASE };

void trigger_external_trigger(void)
{
	vaddr_t base = io_pa_or_va(&pbase_gpio, 0x4000);

	io_setbits32(base + GPIO4_DR_OFFSET, 0b1 << 30);
	udelay(20);
	io_clrbits32(base + GPIO4_DR_OFFSET, 0b1 << 30);
}