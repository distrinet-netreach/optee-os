#include <printk.h>
#include <trace.h>
#include <console.h>
#include <malloc.h>
#include <initcall.h>
#include <mm/core_memprot.h>
#include <io.h>
#include <trace.h>

#include <drivers/spi_eth.h>
#include <drivers/ecspi_ifc.h>
#include <stdint.h>
#include <string.h>

#define REGS_ECSPI1_BASE 0x02008000
static struct io_pa_va pbase = { .pa = REGS_ECSPI1_BASE };

#define W5500_COMMON_REG  0x00
#define W5500_SOCK_REG(n) 0x4 * n + 0x01
#define W5500_SOCK_TX(n)  0x4 * n + 0x02
#define W5500_SOCK_RX(n)  0x4 * n + 0x03

#define W5500_WRITE_CMD(reg) (reg) << 3 | 0x1 << 2
#define W5500_READ_CMD(reg)  (reg) << 3 | 0x0 << 2

#define W5500_MR       0x0
#define W5500_GAR      0x1
#define W5500_SUBR     0x5
#define W5500_SHAR     0x9
#define W5500_SIPR     0xf
#define W5500_INTLEVEL 0x13
#define W5500_IR       0x15
#define W5500_IMR      0x16
#define W5500_SIR      0x17
#define W5500_SIMR     0x18
#define W5500_RTR      0x19
#define W5500_RCR      0x1b
#define W5500_PTIMER   0x1c
#define W5500_PMAGIC   0x1d
#define W5500_PHAR     0x1e
#define W5500_PSID     0x24
#define W5500_PMRU     0x26
#define W5500_UIPR     0x28
#define W5500_UPORTR   0x2c
#define W5500_PHYCFGR  0x2e
#define W5500_VERSIONR 0x39

#define W5500_SN_MR	    0x0
#define W5500_SN_CR	    0x1
#define W5500_SN_IR	    0x2
#define W5500_SN_SR	    0x3
#define W5500_SN_PORT	    0x4
#define W5500_SN_DHAR	    0x6
#define W5500_SN_DIPR	    0xc
#define W5500_SN_DPORT	    0x10
#define W5500_SN_MSSR	    0x12
#define W5500_SN_TOS	    0x15
#define W5500_SN_TTL	    0x16
#define W5500_SN_RXBUF_SIZE 0x1e
#define W5500_SN_TXBUF_SIZE 0x1f
#define W5500_SN_TX_FSR	    0x20
#define W5500_SN_TX_RD	    0x22
#define W5500_SN_TX_WR	    0x24
#define W5500_SN_RX_RSR	    0x26
#define W5500_SN_RX_RD	    0x28
#define W5500_SN_RX_WR	    0x2a
#define W5500_SN_IMR	    0x2c
#define W5500_SN_FRAG	    0x2d
#define W5500_SN_KPALVTR    0x2f

TEE_Result open_ethernet(void)
{
	TEE_Result ret = TEE_SUCCESS;
	vaddr_t base = io_pa_or_va(&pbase, 0x4000);

	const param_ecspi_t params = {
		.channel = 0, //!< SS channel
		.mode = 1, //!< 0: slave, 1: master
		.ss_pol = 0, //!< 0: active low, 1: active high
		.sclk_pol = 0, //!< 0: active high, 1: active low
		.sclk_pha = 0, //!< 0: phase 0 op, 1: phase 1 op
		.pre_div = 1, //!< Clock pre-divider, from 1-16
		.post_div =
			6, //!< Clock post-divider, from 0-15, actual divisor = 2^post_div
		._reserved = 0, //!< Reserved.
	};
	if (core_pbuf_is(CORE_MEM_CACHED, pbase.pa, 0x4000)) {
		EMSG("The memory of ECSPI1 is cached!");
		return TEE_ERROR_BAD_STATE;
	}
	ecspi_open(base, &params);

	uint16_t offset = 0x39;
	uint8_t cmd = W5500_READ_CMD(W5500_COMMON_REG);
	uint8_t val;
	ecspi_read(cmd, offset, &val, 1);
	if (val != 0x4) {
		EMSG("SPI connection has problems!");
		IMSG("Return: %x", val);
		ret = TEE_ERROR_BAD_STATE;
	}

	verify_iomux();

	return ret;
}

TEE_Result configure_ethernet(uint8_t mac[], uint8_t ip[], uint8_t gateway[], uint8_t subnet[])
{
	// Reset W5500
	uint8_t val = 0b10000000;
	uint16_t offset = W5500_MR;
	uint8_t cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, &val, 1);

	// Set hardware address
	offset = W5500_SHAR;
	cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, mac, 6);

	// Set ip address
	offset = W5500_SIPR;
	cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, ip, 4);

	// Set gateway address
	offset = W5500_GAR;
	cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, gateway, 4);

	// Set subnet mask
	offset = W5500_SUBR;
	cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, subnet, 4);

	// Configure socket 0
	val = 0b00000100;
	offset = W5500_SN_MR;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset, &val, 1);

	val = 0b1; // Enable socket 0 interrupt
	offset = W5500_SIMR;
	cmd = W5500_WRITE_CMD(W5500_COMMON_REG);
	ecspi_write(cmd, offset, &val, 1);

	val = 0b100; // Enable socket 0 RECV interrupt
	offset = W5500_SN_IMR;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset, &val, 1);

	// Set Rx and Tx buf size to 16kB
	val = 16;
	offset = W5500_SN_RXBUF_SIZE;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset, &val, 1);
	offset = W5500_SN_TXBUF_SIZE;
	ecspi_write(cmd, offset, &val, 1);

	// Open socket 0
	val = 0x1;
	offset = W5500_SN_CR;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset, &val, 1);
	// Ensure command has completed
	offset = W5500_SN_SR;
	cmd = W5500_READ_CMD(W5500_SOCK_REG(0));
	do {
		ecspi_read(cmd, offset, &val, 1);
		DMSG("SN_SR: %x", val);
	} while (val != 0x42);

	return TEE_SUCCESS;
}

TEE_Result reset_interrupt(void)
{
	uint8_t val = 0b100;
	uint16_t offset = W5500_SN_IR;
	uint8_t cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset, &val, 1);
	return TEE_SUCCESS;
}

TEE_Result send_packet(uint8_t *packet_ptr, uint16_t len)
{
	uint16_t offset = W5500_SN_TX_FSR;
	uint8_t cmd = W5500_READ_CMD(W5500_SOCK_REG(0));
	uint8_t read_buf[2] = { 0, 0 };
	ecspi_read(cmd, offset, (uint8_t *)read_buf, 2);

	offset = W5500_SN_TX_WR;
	cmd = W5500_READ_CMD(W5500_SOCK_REG(0));
	ecspi_read(cmd, offset, (uint8_t *)read_buf, 2);
	uint16_t write_pointer = read_buf[0] << 8 | (read_buf[1] & 0xff);

	cmd = W5500_WRITE_CMD(W5500_SOCK_TX(0));
	write_pointer += ecspi_write(cmd, write_pointer, packet_ptr, len);

	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset,
		    (uint8_t *)&(uint8_t[2]){ write_pointer >> 8,
					      write_pointer & 0xff },
		    2);

	offset = W5500_SN_CR;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	uint8_t val = 0x20;
	ecspi_write(cmd, offset, &val, 1);

	// Wait for send command to complete
	cmd = W5500_READ_CMD(W5500_SOCK_REG(0));
	offset = W5500_SN_IR;
	do {
		ecspi_read(cmd, offset, &val, 1);
	} while (!(val & 0x10));

	return TEE_SUCCESS;
}

TEE_Result recv_packets(uint8_t **packet_ptr, uint16_t *len)
{
	uint16_t offset = W5500_SN_RX_RSR;
	uint8_t cmd = W5500_READ_CMD(W5500_SOCK_REG(0));
	uint8_t read_buf[2] = { 0, 0 };
	uint16_t size = 0;
	do {
		*len = size;
		ecspi_read(cmd, offset, (uint8_t *)read_buf, 2);
		size = read_buf[0] << 8 | read_buf[1];
	} while (size != *len);
	*len = size;
	if (!*len) {
		return TEE_SUCCESS;
	}

	offset = W5500_SN_RX_RD;
	ecspi_read(cmd, offset, (uint8_t *)read_buf, 2);
	uint16_t read_pointer = read_buf[0] << 8 | read_buf[1];

	offset = read_pointer;
	cmd = W5500_READ_CMD(W5500_SOCK_RX(0));
	*packet_ptr = malloc(*len);
	ecspi_read(cmd, offset, *packet_ptr, *len);

	read_pointer += *len;
	offset = W5500_SN_RX_RD;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	ecspi_write(cmd, offset,
		    (uint8_t *)&(uint8_t[2]){ read_pointer >> 8,
					      read_pointer & 0xff },
		    2);

	offset = W5500_SN_CR;
	cmd = W5500_WRITE_CMD(W5500_SOCK_REG(0));
	uint8_t val = 0x40;
	ecspi_write(cmd, offset, &val, 1);

	return TEE_SUCCESS;
}
