#include <drivers/serial.h>

#ifndef SECURE_SSP_DRIVER
#define SECURE_SSP_DRIVER

void register_serial_chip_con_split(struct serial_chip *chip);

#endif /* SECURE_SSP_DRIVER */
