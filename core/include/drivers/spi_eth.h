#ifndef SPI_ETH
#define SPI_ETH

#include <tee_api_types.h>

TEE_Result configure_clocks(void);
TEE_Result configure_iomux(void);
TEE_Result verify_iomux(void);

TEE_Result open_ethernet(void);
TEE_Result configure_ethernet(uint8_t mac[], uint8_t ip[], uint8_t gateway[], uint8_t subnet[]);
TEE_Result send_packet(uint8_t *packet_ptr, uint16_t len);
TEE_Result recv_packets(uint8_t **packet_ptr, uint16_t *len);
TEE_Result reset_interrupt(void);

#endif // SPI_ETH