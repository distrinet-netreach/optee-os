#ifndef SECURE_ETHERNET_DRIVER_PUBLIC
#define SECURE_ETHERNET_DRIVER_PUBLIC

#define PTA_NAME "Secure world ssp eth driver"
#define UUID1	 0x82673b48
#define UUID2	 0x3cd2
#define UUID3	 0x4a15
#define UUID4	 0x93
#define UUID5	 0x28
#define UUID6	 0xb5
#define UUID7	 0x59
#define UUID8	 0xdf
#define UUID9	 0xb0
#define UUID10	 0x72
#define UUID11	 0xab

/*
 * Send/receive ethernet packets
 *
 * [out]     memref[0]        Array of device UUIDs
 *
 * Return codes:
 * TEE_SUCCESS - Invoke command success
 * TEE_ERROR_BAD_PARAMETERS - Incorrect input param
 * TEE_ERROR_SHORT_BUFFER - Output buffer size less than required
 */
#define SEC_ETH_CMD_SEND 0x0
#define SEC_ETH_CMD_RCV	 0x1
#define SEC_ETH_CMD_INIT 0x2
#define SEC_ETH_CMD_PING 0x3

#endif /* SECURE_ETHERNET_DRIVER_PUBLIC */
