#ifndef SECURE_ETHERNET_DRIVER
#define SECURE_ETHERNET_DRIVER

#include <stdint.h>
#include <tee_internal_api.h>

typedef int (*eth_recv_callback_t)(void* data_ptr, int16_t data_size);

TEE_Result secure_ethernet_driver_send(void);

// int secure_ethernet_driver_register_callback(eth_recv_callback_t callback);

// bool secure_ethernet_driver_get_link_status(void);


#endif /* SECURE_ETHERNET_DRIVER */
