#ifndef __ECSPI_IFC_HDR__
#define __ECSPI_IFC_HDR__

#include <types_ext.h>

//! @brief eCSPI port configuration settings.
//!
//! The pre-divider value should be the actual desired divider values. That is,
//! use a value of 1 to divide by 1, a value of 4 to divide by 4, and so on. This may
//! seem obvious, but the eCSPI divider register is set to the divisor minus 1 such that
//! a value of 0 equals an actual divisor of 1. In contrast, the post divider is the power
//! of 2 that will be divided by. For instance, a post_div of 3 will divide by 2^3=8.
//! pre_div ranges from 1-16, while post_div ranges from 0-15 (thus resulting in a divisor
//! of 2^0=1 through 2^15=65536).
typedef struct {
	unsigned channel : 2; //!< SS channel
	unsigned mode : 1; //!< 0: slave, 1: master
	unsigned ss_pol : 1; //!< 0: active low, 1: active high
	unsigned sclk_pol : 1; //!< 0: active high, 1: active low
	unsigned sclk_pha : 1; //!< 0: phase 0 op, 1: phase 1 op
	unsigned pre_div : 5; //!< Clock pre-divider, from 1-16
	unsigned post_div : 4; //!< Clock post-divider, from 0-15, actual divisor = 2^post_div
	unsigned _reserved : 17; //!< Reserved.
} param_ecspi_t;

/*!
 * @brief Open and initialize eCSPI device
 *
 * @param dev eCSPI port number.
 * @param param Device configuration.
 *
 * @return TRUE on success, FALSE on failure
 */
int ecspi_open(vaddr_t base, const param_ecspi_t *param);

/*!
 * @brief Close/disable eCSPI device.
 *
 * @param dev eCSPI port.
 *
 * @return TRUE on success, FALSE on failure
 */
int ecspi_close(vaddr_t base);

/*!
 * @brief Reconfigure eCSPI device.
 *
 * @param instance eCSPI port number.
 * @param param Device configuration.
 *
 * @return TRUE on success, FALSE on failure
 */
int ecspi_configure(vaddr_t base, const param_ecspi_t *param);

/*!
 * @brief Transfer from/to target device.
 *
 * @param device eCSPI port.
 * @param tx_buf Source buffer that contain data to send.
 * @param rx_buf Buffer that store data received.
 * @param brs_bts Burst length in bits.
 *
 * @return TRUE on success, FALSE on failure
 */
int ecspi_xfer(vaddr_t base, const uint8_t *tx_buf, uint8_t *rx_buf,
	       uint16_t brs_bts);

uint16_t ecspi_read(uint8_t command, uint16_t offset, volatile uint8_t *const buf,
		    uint16_t bytes);

uint16_t ecspi_write(uint8_t command, uint16_t offset, uint8_t *const buf,
		     uint16_t bytes);

void trigger_external_trigger(void);

#endif // __ECSPI_IFC_HDR__
